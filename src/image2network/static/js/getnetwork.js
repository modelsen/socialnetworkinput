var scanUrl = "http://127.0.0.1:8000/scanImage/"
var apiPOST = "http://127.0.0.1:8000/processImage/";

let button = document.getElementById("startbutton");
let refresh = document.getElementById("refresh");


function refreshColor() {
    let colorVal = document.getElementById("hex").innerHTML;
    let hueVal = document.getElementById("hue").innerHTML;
    let camVal = document.getElementById("cam").innerHTML;
    let parameter = '?color=';
    window.location.search = parameter.concat(colorVal.substring(1)).concat("&hueDelta=").concat(hueVal).concat("&camera=").concat(camVal);
    console.log("Refreshed color and hue delta.")
};

function processImage() {
    let colorVal = document.getElementById("hex").innerHTML;
    let hueVal = document.getElementById("hue").innerHTML;
    let camVal = document.getElementById("cam").innerHTML;
    let parameterString = '?color='.concat(colorVal.substring(1)).concat("&hueDelta=").concat(hueVal).concat("&camera=").concat(camVal);
    let fullURL = apiPOST.concat(parameterString);
    let xhr = new XMLHttpRequest();
    xhr.open("POST", fullURL);
    xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.send();
    xhr.onreadystatechange = function(){
        if(xhr.readyState != 4) return;
        if(xhr.status != 200){
            alert("Status: " + xhr.status + xhr.responseText);
        }else{
            console.log('done');
        }
    };
};
refresh.addEventListener('click', function(ev){
    refreshColor();
    ev.preventDefault();
    }, false);

button.addEventListener('click', function(ev){
    let colorVal = document.getElementById("hex").innerHTML;
    let hueVal = document.getElementById("hue").innerHTML;
    let camVal = document.getElementById("cam").innerHTML;
    let parameterString = '?color='.concat(colorVal.substring(1)).concat("&hueDelta=").concat(hueVal).concat("&camera=").concat(camVal);
    let fullURL = apiPOST.concat(parameterString);
    window.open(fullURL, target="_self");
    }, false);

var theInput = document.getElementById("kb_selected_color");
var theColor = theInput.value;

theInput.addEventListener("input", function() {
    document.getElementById("hex").innerHTML = theInput.value;
    document.documentElement.style.setProperty('--kb-color',theInput.value);
    }, false);

var hueInput = document.getElementById("huerange");
var huerange = hueInput.value;

hueInput.addEventListener("input", function() {
    document.getElementById("hue").innerHTML = hueInput.value;
    }, false);

var camInputElement = document.getElementById("cameraInput");
var cameraValue = camInputElement.options[camInputElement.selectedIndex].text;

camInputElement.addEventListener("change", function() {
    document.getElementById("cam").innerHTML = camInputElement.value;
    }, false);