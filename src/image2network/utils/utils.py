import cv2
import networkx as nx
import numpy as np
from typing import Tuple
from PIL import Image, ImageColor
from skimage import morphology, segmentation
from skimage.morphology import skeletonize, thin
import base64



def highlight_color(img: np.ndarray, rgb: Tuple[int]) -> np.ndarray:
    """Replace color in image, to highlight selected color choice."""
    out = img.copy()
    r, g, b = rgb
    out[(img[:, :, 0] == r) & (img[:, :, 1] == g) & (img[:, :, 2] == b)] = 255
    return out


def maskColor(data: np.ndarray, color: str, hueDelta: int = 20):
    r, g, b = ImageColor.getcolor(color, "RGB")
    targetColor = np.uint8([[[b, g, r]]])
    targetColor = cv2.cvtColor(targetColor, cv2.COLOR_BGR2HSV)
    hue = targetColor[0][0][0]
    lower_color = np.array([hue - hueDelta, 100, 100]) 
    upper_color = np.array([hue + hueDelta, 255, 255])
    hsv = cv2.cvtColor(data, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, lower_color, upper_color)  
    coord = cv2.findNonZero(mask)
    res = cv2.bitwise_and(data, data, mask=mask)
    return res, coord


def returnSkeleton(data: np.ndarray, sigma: int = 5):
    grayFrame = cv2.cvtColor(data, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(
        grayFrame,
        (0, 0),
        sigmaX=sigma,
        borderType=cv2.BORDER_DEFAULT
    )
    _, thresholdFrame = cv2.threshold(blur, 128, 192, cv2.THRESH_OTSU)
    skel = morphology.skeletonize(thresholdFrame)
    thinned = thin(thresholdFrame)
    _, skelImg = cv2.imencode(".png", skel * 255)
    _, thinImg = cv2.imencode(".png", thinned * 255)
    return skelImg, skel, thinImg, thinned


def getImage(camera: int=0, resolution: tuple=(320, 240)):
    camera = cv2.VideoCapture(f"/dev/video{camera}")
    camera.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
    camera.set(cv2.CAP_PROP_FRAME_HEIGHT, 240)
    success, frame = camera.read()
    camera.release()
    ret, buffer = cv2.imencode('.png', frame)  
    img = base64.b64encode(buffer).decode("utf-8")
    return success, img, frame


def cameraDetection():
    cams = []
    cams_test = 20
    for i in range(0, cams_test):
        cap = cv2.VideoCapture(f"/dev/video{i}")
        test, frame = cap.read()
        if test:
            cams.append(i)
    return cams


def drawNetwork(
    data: np.ndarray,
    graph: nx.Graph,
    color: tuple = (255, 255, 255)
):
    img = data.copy()
    for (n1, n2, k) in graph.edges(keys=True):
        path = graph[n1][n2][k]['path']
        for pt1, pt2 in zip([*path.coords][:-1], [*path.coords][1:]):
            x1, y1 = pt1
            x2, y2 = pt2
            cv2.line(
                img,
                (int(y1), int(x1)),
                (int(y2), int(x2)),
                color=color,
                thickness=1
            )
    for x, y in graph.nodes():
        cv2.circle(
            img,
            (y, x),
            5,
            (0, 0, 0),
            2
        )
    ret, buffer = cv2.imencode('.png', img)
    img = base64.b64encode(buffer).decode("utf-8")  
    return img

def multi2simpleGraph(g):
    G = nx.Graph()
    for u,v,data in g.edges(data=True):
        w = data['weight'] if 'weight' in data else 1.0
        if G.has_edge(u,v):
            G[u][v]['weight'] += w
        else:
            G.add_edge(u, v, weight=w)
    return G