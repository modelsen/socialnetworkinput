import signal
import base64
from typing import Any, Annotated
from pathlib import Path
from fastapi import (
    FastAPI,
    Query,
    Request
)
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

import uvicorn
import networkx as nx


from image2network.utils.utils import (
    maskColor,
    returnSkeleton,
    getImage,
    drawNetwork,
    cameraDetection,
    multi2simpleGraph
)
from image2network.utils.getNetwork import (
    extract_network
)


def handleTimeout(signum, frame):
    """Handle long running network measures."""
    raise TimeoutError("Takes to long.")


cameras = cameraDetection()

app = FastAPI()

BASE_PATH = Path(__file__).resolve().parent
DATA_PATH = BASE_PATH / "static" / "data"
app.mount(str(BASE_PATH / "static"), StaticFiles(directory=str(BASE_PATH / "static")), name="static")
templates = Jinja2Templates(directory=str(BASE_PATH / "templates"))

navMenu = [
    {'caption': "Home", "href": "/"},
    {'caption': "Scan", "href": "/scanImage"},
]


@app.get('/')
async def index(request: Request):
    return templates.TemplateResponse(
        "startpage.html",
        {
            "request": request,
            "navigation": navMenu,
            'header': 'Draw your social network',
            'text': 'This website is an interface between a drawn network image and its representation in the computer.'
        }
    )


@app.get("/scanImage/")
async def get_image(
    request: Request,
    color: Annotated[str, Query(length=6)] = '000000',
    hueDelta: str = '2',
    camera: str = str(cameras[-1]),
) -> Any:
    """
    Fetch a camera image
    """
    success, imgEnc, frame = getImage(camera=int(camera))
    
    if not success:
        return templates.TemplateResponse(
            "getImage.html",
            {"request": request, "navigation": navMenu, 'message': 'Error'},
        )
    else: 
        # Masked based on the selected color
        maskedFrame, _ = maskColor(frame, f"#{color}", int(hueDelta))
        skelBuffer, skeleton, thinImg, thinned = returnSkeleton(maskedFrame)
        thinnedImageEnc = base64.b64encode(thinImg).decode("utf-8")
        return templates.TemplateResponse(
            "getImage.html",
            {
                "request": request,
                "navigation": navMenu,
                'image': imgEnc,
                'mask': thinnedImageEnc,
                'color': color,
                'huedelta': hueDelta,
                'camera': camera,
                'cameras': cameras,
                'header': 'Load image',
                'text': 'To reload an image of the webcam, just refresh this page.'
            },
        )


@app.get("/processImage/")
async def processImage(
    request: Request,
    color: Annotated[str, Query(length=6)] = '000000',
    hueDelta: str = '2',
    camera: str = str(cameras[-1])
):
    """
    Extract the social network from the image
    """
    success, imgEnc, frame = getImage(camera=int(camera), resolution=(1920, 1080))
    
    maskedFrame, _ = maskColor(frame,  f"#{color}", int(hueDelta))

    skelImg, skel, thinImg, thinned = returnSkeleton(maskedFrame)
    thinImgEnc = base64.b64encode(skelImg).decode("utf-8")
    g = extract_network(thinned, min_distance=10)
    G = multi2simpleGraph(g)
    measures = {
        "smallworld": lambda x: nx.algorithms.smallworld.sigma(x),
        "numMinEdgeCut": lambda x: len(list(nx.connectivity.minimum_edge_cut(x))),
        "AvShortPath": lambda x: nx.average_shortest_path_length(x),
    }
    measureVal = {}
    for key, val in measures.items():
        signal.signal(signal.SIGALRM, handleTimeout)
        signal.alarm(10)
        try:
            res = val(G)
        except Exception as exc:
            print(exc)
            res = "This takes time!"
        measureVal[key] = res
        signal.alarm(0)


    networkFrame = drawNetwork(frame, g, color=(255, 255, 255))
    return templates.TemplateResponse(
        "getNetwork.html",
        {
            "request": request,
            "navigation": navMenu,
            'mask': networkFrame,
            'color': color,
            'huedelta': hueDelta,
            'nodes': len(G.nodes()),
            "edges": len(G.edges()),
            "smallworld": measureVal["smallworld"],
            "numMinEdgeCut": measureVal["numMinEdgeCut"],
            "AvShortPath": measureVal["AvShortPath"],
            "header": "Statistics of the network.",
        }
    )


if __name__ == '__main__':
    uvicorn.run(app, host='127.0.0.1', port=8000)
