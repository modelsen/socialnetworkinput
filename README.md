# image2network

This app allows to read in drawn networks and transforms them to computer representations. These can then be used as initial conditions for simulations or be analyzed for their network properties.

## Local installation

The app is build with fastapi, which uses uvicorn. To locally run the app, first clone the repository

```bash
git clone https://gitlab.gwdg.de/modelsen/socialnetworkinput
```

In the cloned folder then start a new virtual environment and install the app.

```bash
python3 -m venv env
source env/bin/activate
pip install -U .
```

## Running the interface

In the activated environment then change in the folder `src/image2network` and run
```bash
uvicorn loader:app
```

Opening a local browser to the address [on localhost:8000](http://127.0.0.1:8000) will show the interface. 

__Note__: You will need a webcam and allow localhost to access the webcam. Each page refresh takes an image from the webcam.