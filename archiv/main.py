import base64
import io

from fastapi import FastAPI, APIRouter, Request, File
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

from typing import Any, Annotated
from pathlib import Path

from PIL import Image

app = FastAPI(title="Capture social network", openapi_url="/openapi.json")

BASE_PATH = Path(__file__).resolve().parent
app.mount(str(BASE_PATH / "static"), StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory=str(BASE_PATH / "templates"))

router = APIRouter()

navMenu = [
    {'caption': "Home", "href": "/"},
    {'caption': "Scan", "href": "/scanImage"},
    {'caption': "Simulate", "href": "/runSimulation"}
]


def getImageFromRequest(file):
    """Get image from posted request."""
    filestring = file
    #bytesFormat = base64.decodebytes(filestring)
    try:
        img = Image.open(io.BytesIO(filestring))
        text = "Success" 
    except:
        text = 'Could not read image format.'
    return text


@router.get("/", status_code=200, response_class=HTMLResponse)
async def root(request: Request) -> dict:
    """
    Root GET
    """
    return templates.TemplateResponse(
        "index.html",
        {"request": request, "navigation": navMenu},
    )


@router.get("/scanImage/", status_code=200, response_class=HTMLResponse)
async def get_image(request: Request) -> Any:
    """
    Fetch a camera image
    """
    return templates.TemplateResponse(
        "getImage.html",
        {"request": request},
    )


@router.post("/processImage/")
async def processImage(
    request: Request,
    file: Annotated[bytes, File()]
):
    """
    Extract the social network from the image
    """
    # TODO
    text = getImageFromRequest(file)
    # TODO: Get those colors from image that should show the network, e.g. black, blue, red
    # Run extract network on the resulting image, and follow routines as in getNetwork.py
    return templates.TemplateResponse(
        "index.html",
        {"request": request, "messages": [text]},
    )


@router.get("/runSimulation/", status_code=200, response_class=HTMLResponse)
async def runSimulation(request: Request) -> Any:
    """
    Run simulation with detected network.
    """
    # TODO
    network = ''
    return templates.TemplateResponse(
        "index.html",
        {"request": request, "network": network},
    )

app.include_router(router)


if __name__ == "__main__":
    # Use this for debugging purposes only
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000, log_level="debug")
