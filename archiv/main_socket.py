from fastapi import FastAPI, APIRouter, Request, WebSocket, WebSocketDisconnect
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

import uvicorn
import cv2
from pathlib import Path

app = FastAPI()
camera = cv2.VideoCapture(0, cv2.CAP_DSHOW)

BASE_PATH = Path(__file__).resolve().parent
app.mount(str(BASE_PATH / "static"), StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory=str(BASE_PATH / "templates"))

router = APIRouter()


@router.get('/')
def index(request: Request):
    return templates.TemplateResponse("index_socket.html", {"request": request})


@router.websocket("/ws")
async def get_stream(websocket: WebSocket):
    await websocket.accept()
    try:
        while True:
            success, frame = camera.read()
            if not success:
                break
            else:
                ret, buffer = cv2.imencode('.jpg', frame)
                await websocket.send_bytes(buffer.tobytes())  
    except WebSocketDisconnect:
        print("Client disconnected")   

app.include_router(router)


if __name__ == '__main__':
    uvicorn.run(app, host='127.0.0.1', port=8000, log_level="debug")